﻿#pragma once
#include "BuildingEnum.generated.h"

UENUM()
enum EBuildingType
{
	Building_Source,
	Building_Transmitter,
	Building_Portal
};

UENUM()
enum ENewLevelDirection
{
	LEFT,
	RIGHT
};

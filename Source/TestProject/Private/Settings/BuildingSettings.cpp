﻿#include "Settings/BuildingSettings.h"

UBuildingSettings::UBuildingSettings()
{
	ResourcePerTime = 1;
	ResourceAddFrequency = 1;
	BuildingClasses = {};
}

UBuildingSettings* UBuildingSettings::GetBuildingSettings()
{
	return GetMutableDefault<UBuildingSettings>();
}
